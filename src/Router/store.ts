import { writable, Writable } from 'svelte/store';

export const curRoute: Writable<string> = writable('/');