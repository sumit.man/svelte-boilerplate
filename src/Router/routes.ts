import { IRoute } from "./interfaces";

import Home from '../views/Home.svelte';
import About from '../views/About.svelte';

const routes: IRoute[] = [
	{
		path: '/',
		name: 'Home',
		component: Home,
	},
	{
		path: '/about',
		name: 'About',
		component: About,
	},
];

export default routes;